<?php
session_start();
$usernamelogged=$_SESSION['usernamelogged'];
include('connect.php');
date_default_timezone_set("Asia/kolkata");
$startmonth=date('Y-m-01');
$endmonth=date('Y-m-t');
$sql = mysqli_query($db_con,"SELECT * FROM newcase WHERE adusername='$usernamelogged' AND casedate BETWEEN '$startmonth' AND '$endmonth'");
?>
<html>
<head>
  <style>
  table, th, td {
      border: 1px solid black;
      border-collapse: collapse;
  }
  th, td {
      padding: 5px;
      text-align: left;
  }
  </style>
  </head>
  <body>

  <table style="width:100%">
    <caption>Cases This Month</caption>
    <tr>
      <th>Case Number</th>
      <th>Case Name</th>
      <th>Court Name</th>
      <th>Case Stage</th>
      <th>Client contact</th>
      <th>Next Hearing</th>
    </tr>
    <?php
      while($array=mysqli_fetch_array($sql,MYSQLI_ASSOC))
      {
    ?>
    <tr>
    <td><?php echo $array['casenumber'];?></td>
    <td><?php echo $array['casename'];?></td>
    <td><?php echo $array['courtname']; ?></td>
    <td><?php echo $array['casestatus']; ?></td>
    <td><?php echo $array['contactno']; ?></td>
    <td><?php echo $array['casedate']; ?></td>
    </tr>
    <?php } ?>
  </table>

  </body>
  </html>
