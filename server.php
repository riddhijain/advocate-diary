<?php
session_start();
$username = "";
$email    = "";
$errors = array();
include('connect.php');
include('errors.php');
if (isset($_POST['reg_user'])) {
  // receive all input values from the form
  $username = mysqli_real_escape_string($db_con, $_POST['username']);
    $adusername = mysqli_real_escape_string($db_con, $_POST['username']);
  $email = mysqli_real_escape_string($db_con, $_POST['email']);
  $password_1 = mysqli_real_escape_string($db_con, $_POST['password_1']);
  $password_2 = mysqli_real_escape_string($db_con, $_POST['password_2']);

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($username)) { array_push($errors, "Username is required"); }
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if ($password_1 != $password_2) {
	array_push($errors, "The two passwords do not match");
  }

  // first check the database to make sure
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM users WHERE username='$username' OR email='$email' LIMIT 1";
  $result = mysqli_query($db_con, $user_check_query);
  $user = mysqli_fetch_assoc($result);

  if ($user) { // if user exists
    if ($user['username'] === $username) {
      array_push($errors, "Username already exists");
    }

    if ($user['email'] === $email) {
      array_push($errors, "email already exists");
    }
  }

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
  	$password = md5($password_1);//encrypt the password before saving in the database
  	$query = "INSERT INTO users (username, email, password)
  			  VALUES('$username', '$email', '$password')";
  	$x=mysqli_query($db_con, $query);

  	$_SESSION['username'] = $username;
  	$_SESSION['success'] = "You are now registered";
    echo '<script language="javascript">';
      echo 'alert("congratulation! you are now registered")';
      echo '</script>';
        echo "<script>setTimeout(\"location.href = 'login.php';\",1500);</script>";

  //  if((!mysqli_query($db_con,$query))||(!mysqli_query($db_con,$query2))){
    //  die('error inserting the record');
  }
}

// LOGIN USER
if (isset($_POST['login_user'])) {
  $usernamelogged = mysqli_real_escape_string($db_con, $_POST['username']);
  $passwordlogged = mysqli_real_escape_string($db_con, $_POST['password']);
  if (empty($usernamelogged)) {

    echo '<script language="javascript">';
      echo 'alert("username is required")';
      echo '</script>';
        echo "<script>setTimeout(\"location.href = 'login.php';\",1500);</script>";
  }
  if (empty($passwordlogged)) {
      echo '<script language="javascript">';
        echo 'alert("Password is required")';
        echo '</script>';
          echo "<script>setTimeout(\"location.href = 'login.php';\",1500);</script>";
  }

  if (count($errors) == 0)
  {
  	$passwordlogged = md5($passwordlogged);
  	$query = "SELECT * FROM users WHERE username='$usernamelogged' AND password='$passwordlogged'";
  	$results = mysqli_query($db_con, $query);
  	if (mysqli_num_rows($results)== 1) {
  	  $_SESSION['usernamelogged'] = $usernamelogged;
      echo '<script language="javascript">';
        echo 'alert("successfully logged in")';
        echo '</script>';
          include_once('sendmail.php');
          echo "<script>setTimeout(\"location.href = 'index.html';\",1500);</script>";

  	  $_SESSION['success'] = "You are now logged in";
  	}
    else {
          echo '<script language="javascript">';
            echo 'alert("wrong username or password! try aagain")';
            echo '</script>';
              echo "<script>setTimeout(\"location.href = 'login.php';\",1500);</script>";

  	}
  }
}
?>
